Description: create debian template config file
Author: Nicolas Mora <nicolas@babelouest.org>
Index: glewlwyd-1.4.4/docs/glewlwyd.conf.properties
===================================================================
--- /dev/null	2018-06-10 15:33:05.340014242 -0400
+++ glewlwyd-1.4.4/docs/glewlwyd.conf.properties	2018-06-11 15:45:40.000000000 -0400
@@ -0,0 +1,225 @@
+
+# port to open for remote commands
+port=4593
+
+# url prefix
+url_prefix="api"
+
+# path to static files for /app url
+static_files_path="/usr/share/glewlwyd/webapp/"
+
+# static files prefix
+static_files_prefix = "app"
+
+# mime types for static files
+static_files_mime_types =
+(
+  {
+    extension = ".html"
+    type = "text/html"
+  },
+  {
+    extension = ".css"
+    type = "text/css"
+  },
+  {
+    extension = ".js"
+    type = "application/javascript"
+  },
+  {
+    extension = ".png"
+    type = "image/png"
+  },
+  {
+    extension = ".jpg"
+    type = "image/jpeg"
+  },
+  {
+    extension = ".jpeg"
+    type = "image/jpeg"
+  },
+  {
+    extension = ".ttf"
+    type = "font/ttf"
+  },
+  {
+    extension = ".woff"
+    type = "font/woff"
+  },
+  {
+    extension = ".woff2"
+    type = "font/woff2"
+  },
+  {
+    extension = ".map"
+    type = "application/octet-stream"
+  },
+  {
+    extension = ".ico"
+    type = "image/x-icon"
+  }
+)
+
+# access-control-allow-origin value
+allow_origin="*"
+
+# log mode (console, syslog, journald, file)
+log_mode="journald,file"
+
+# log level: NONE, ERROR, WARNING, INFO, DEBUG
+log_level="INFO"
+
+# output to log file (required if log_mode is file)
+log_file="/var/log/glewlwyd.log"
+
+# use scope for access control (default true)
+use_scope=true
+
+# refresh token expiration in seconds, default is 2 weeks
+refresh_token_expiation=1209600
+
+# access token expiration in seconds, default is 1 hour
+access_token_expiration=3600
+
+# session expiration, default is 4 weeks
+session_expiration=2419200
+
+# session key
+session_key="GLEWLWYD_SESSION_ID"
+
+# admin scope name
+admin_scope="g_admin"
+
+# profile scope name
+profile_scope="g_profile"
+
+# additional property name that will be included in access tokens, uncomment to enable this feature
+# additional_property_name  = "new_property"
+
+# code expiration in seconds, default is 10 minutes
+code_expiration=600
+
+# TLS/SSL configuration values
+use_secure_connection=false
+secure_connection_key_file="/etc/glewlwyd/cert.key"
+secure_connection_pem_file="/etc/glewlwyd/cert.pem"
+
+# login and grant urls
+login_url="_G_EXTRNAL_URL_/app/login.html?"
+grant_url="_G_EXTRNAL_URL_/app/grant.html?"
+
+# Reset password configuration
+reset_password=_G_RESET_PWD_FLAG_ # optional, default false
+reset_password_config = 
+{
+# SMTP parameters
+  smtp_host="_G_RESET_PWD_HOST_" # mandatory
+  smtp_port=25                   # optional, default 25
+  smtp_use_tls=false             # optional, default false
+  smtp_verify_certificate=false  # optional, default false
+#  smtp_user="user"              # optional, default no value
+#  smtp_password="password"      # optional, default no value
+  
+  token_expiration=604800                                                                     # in seconds, optional, default 7 days
+  email_from="_G_RESET_PWD_FROM_"                                                           # mandatory
+  email_subject="_G_RESET_PWD_SUBJECT_"                                                     # mandatory
+  email_template="/etc/glewlwyd/reset.eml"                                                                  # mandatory
+  page_url_prefix="_G_EXTRNAL_URL_/app/reset.html?user=$USERNAME&code=$TOKEN"    # mandatory
+}
+
+# Algorithms available are SHA1, SHA256, SHA512, MD5, default is SHA256
+hash_algorithm = "SHA256"
+
+# Database connection
+@include "/etc/glewlwyd/glewlwyd-db.conf"
+
+# Authentication configuration
+authentication =
+{
+   database_auth            = true
+   
+   http_auth                   = false
+   http_auth_url               = "https://example.com/auth/validate" # mandatory if http_auth is true set this value to "http://localhost:2884/auth" for unit tests
+   http_auth_check_certificate = true                                # optional, default true
+	 
+   ldap_auth                = false
+   uri                      = "ldap://localhost"
+   bind_dn                  = "cn=operator,dc=example,dc=org"
+   bind_passwd              = "password"
+   search_scope             = "subtree" # optional, scope of the LDAP search, values available are "onelevel", "subtree" or "children", default is "onelevel", see the manpage ldap_search(3) for more information
+   base_search_user         = "ou=user,dc=example,dc=org"
+   base_search_client       = "ou=client,dc=example,dc=org"
+   
+# Users read parameters
+   filter_user_read               = "objectClass=inetOrgPerson"
+   login_property_user_read       = "cn"
+   scope_property_user_read       = "o"
+   scope_property_user_match      = "equals" # optional, search match for the scope value, values available are "equals", "contains", "startswith" or "endswith", default value is "equals"
+   email_property_user_read       = "mail"
+   name_property_user_read        = "sn"
+   additional_property_value_read = "memberOf" # will fill the jwt property `group` with the content of the LDAP property `memberOf`, optional, leave empty if no use
+# Users write parameters
+   ldap_user_write                 = true
+   rdn_property_user_write         = "cn"           # Single value
+   login_property_user_write       = "cn"           # Multiple values separated by a comma
+   scope_property_user_write       = "o"            # Multiple values separated by a comma
+   email_property_user_write       = "mail"         # Multiple values separated by a comma
+   additional_property_value_write = "memberOf"     # Multiple values separated by a comma
+   name_property_user_write        = "sn"
+   password_property_user_write    = "userPassword" # Single value
+   password_algorithm_user_write   = "SSHA"         # Single value, values available are SSHA, SHA, SMD5, MD5 or PLAIN
+   object_class_user_write         = "top,person,organizationalPerson,inetOrgPerson" # Multiple values separated by a comma
+   
+# Clients read parameters
+   filter_client_read                = "objectClass=inetOrgPerson"
+   client_id_property_client_read    = "cn"
+   scope_property_client_read        = "o"
+   scope_property_client_match       = "equals" # optional, search match for the scope value, values available are "equals", "contains", "startswith" or "endswith", default value is "equals"
+   name_property_client_read         = "sn"
+   description_property_client_read  = "description"
+   redirect_uri_property_client_read = "labeledURI"
+   confidential_property_client_read = "employeeType"
+# Clients write parameters
+   ldap_client_write                  = true
+   rdn_property_client_write          = "cn"          # Single value
+   client_id_property_client_write    = "cn"          # Multiple values separated by a comma
+   scope_property_client_write        = "o"           # Multiple values separated by a comma
+   name_property_client_write         = "sn"          # Multiple values separated by a comma
+   description_property_client_write  = "description" # Multiple values separated by a comma
+   redirect_uri_property_client_write = "labeledURI"  # Multiple values separated by a comma
+   password_property_client_write     = "userPassword"# Single value
+   confidential_property_client_write = "employeeType"
+   password_algorithm_client_write    = "SSHA"        # Single value, values available are SSHA, SHA, SMD5, MD5 or PLAIN
+   object_class_client_write          = "top,person,organizationalPerson,inetOrgPerson" # Multiple values separated by a comma
+}
+
+# jwt parameters
+jwt =
+{
+   # key size for algorithms, values available are 256, 384 or 512, default 512
+   key_size = _G_JWT_KEY_SIZE_
+   
+   # Use RSA algorithm to sign tokens (asymetric)
+   use_rsa = _G_JWT_RSA_FLAG_
+   
+   # path to the key (private) certificate file to sign tokens
+   rsa_key_file = "/etc/glewlwyd/private-rsa.key"
+   
+   # path to the public certificate file to validate signatures
+   rsa_pub_file = "/etc/glewlwyd/public-rsa.pem"
+   
+   # Use ECDSA algorithm to sign tokens (asymetric)
+   use_ecdsa = _G_JWT_ECDSA_FLAG_
+   
+   # path to the key (private) certificate file to sign tokens
+   ecdsa_key_file = "/etc/glewlwyd/private-ecdsa.key"
+   
+   # path to the public certificate file to validate signatures
+   ecdsa_pub_file = "/etc/glewlwyd/public-ecdsa.pem"
+   
+   # Use SHA algorithm to sign tokens (symetric)
+   use_sha = _G_JWT_SHA_FLAG_
+   
+   # characters used to generate and validate the token
+   sha_secret = "_G_JWT_SHA_SECRET_"
+}
